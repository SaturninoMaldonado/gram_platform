#include <SPI.h>
#include <mcp2515.h>

struct can_frame canMsg, canMsg1;
MCP2515 mcp2515(10);
// 53 para el MEGA
// 10 para el nano


void setup() {
    
  pinMode(9, OUTPUT);


  Serial.begin(115200);
  
  mcp2515.reset();
//  mcp2515.setBitrate(CAN_125KBPS);
    mcp2515.setBitrate(CAN_1000KBPS, MCP_8MHZ);

//  mcp2515.setBitrate(CAN_1000KBPS);
  mcp2515.setNormalMode();
  Serial.println("------- CAN Read ----------");
  Serial.println("ID  DLC   DATA");

  canMsg1.can_id  = 0x120;
  canMsg1.can_dlc = 8;
  canMsg1.data[0] = 0x8E;
  canMsg1.data[1] = 0x87;
  canMsg1.data[2] = 0x32;
  canMsg1.data[3] = 0xFA;
  canMsg1.data[4] = 0x26;
  canMsg1.data[5] = 0x8E;
  canMsg1.data[6] = 0xBE;
  canMsg1.data[7] = 0x86;
}

void loop() 
{
  char incoming_byte = '\0';
  
  if (mcp2515.readMessage(&canMsg) == MCP2515::ERROR_OK) 
  {
    Serial.print(canMsg.can_id, HEX); // print ID
    Serial.print(" "); 
    Serial.print(canMsg.can_dlc, HEX); // print DLC
    Serial.print(" ");
    
    for (int i = 0; i<canMsg.can_dlc; i++)  
    {  // print the data
      Serial.print(canMsg.data[i],HEX);
      Serial.print(" ");
    }
    Serial.print("\n");
  }
   
  incoming_byte = Serial.read();
  if(incoming_byte == '1')
  {
      canMsg1.data[0] = 0x01;
      canMsg1.data[1] = 0x00;
      if (mcp2515.sendMessage(&canMsg1) == MCP2515::ERROR_OK) 
        Serial.print("Messages sent  ");
      else
        Serial.print("Msg1 TX error  ");
  }
  else if(incoming_byte == '2')
  {
      canMsg1.data[0] = 0x00;
      canMsg1.data[1] = 0x01;
      if (mcp2515.sendMessage(&canMsg1) == MCP2515::ERROR_OK) 
        Serial.print("Messages sent  ");
      else
        Serial.print("Msg1 TX error  ");
  }
  else if(incoming_byte == '3')
  {
      canMsg1.data[0] = 0x79;
      canMsg1.data[1] = 0x7A;
      if (mcp2515.sendMessage(&canMsg1) == MCP2515::ERROR_OK) 
        Serial.print("Messages sent  ");
      else
        Serial.print("Msg1 TX error  ");
  }
  else if(incoming_byte == '4')
  {
      canMsg1.data[0] = 0x7B;
      canMsg1.data[1] = 0x01;
      if (mcp2515.sendMessage(&canMsg1) == MCP2515::ERROR_OK) 
        Serial.print("Messages sent  ");
      else
        Serial.print("Msg1 TX error  ");
  }
  else if(incoming_byte == '5')
  {
      canMsg1.data[0] = 0x00;
      canMsg1.data[1] = 0x00;
      if (mcp2515.sendMessage(&canMsg1) == MCP2515::ERROR_OK) 
        Serial.print("Messages sent  ");
      else
        Serial.print("Msg1 TX error  ");
  }
}
