from getkey import getkey, keys
import serial
import time

s = serial.Serial('/dev/ttyUSB1', 115200)
time.sleep(1)

while(1):
    
    key = getkey()
    if key == keys.A:
        s.write(str.encode('1'))
    elif key == keys.B:
        s.write(str.encode('2'))   
    elif key == keys.C:
        s.write(str.encode('3'))
    elif key == keys.D:
        s.write(str.encode('4'))
    elif key == keys.E:
        s.write(str.encode('5'))
    elif key == keys.Q:
        break
    else:
        print(key)
