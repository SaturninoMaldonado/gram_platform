**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

![picture](img/abc.jpg)



![alt text](img/abc.jpg)

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

0. LOLA Board CAN modifications.
1. USB Comands
2. Bluetooth functionality
1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

0. LOLA Board CAN modifications.
![alt text](img/Mod_para_CAN_1.jpg)
![alt text](img/Mod_para_CAN_2.jpg)
![alt text](img/Mod_para_CAN_3.jpg)


## USB Commands
1. USB commands
USB Serial communication 38400 baudios ( en algunas pruebas estamos utilizando 115200 )

0XXXYYYY Rotate unclockwise with defferents speeds (Right wheel faster) XXX is the speed ratios * 999 and YYYY the distance in mm

1XXXYYYY Rotate   clockwise with defferents speeds (Left  wheel faster) XXX is the speed ratios * 999 and YYYY the distance

JXXXYYYY Rotate   clockwise with defferents speeds (Right wheel faster backward) XXX is the speed ratios * 999 and YYYY the distance

KXXXYYYY Rotate unclockwise with defferents speeds (Left  wheel faster backward) XXX is the speed ratios * 999 and YYYY the distance

2XXX     Rotate unclockwise respect to the wheel axis center XXX is the angle to turn in degrees

3XXX     Rotate   clockwise respect to the wheel axis center XXX is the angle to turn in degrees

4XXXX    Move forward  at maximum speed XXXX is the distance in mm

5XXXX    Move backward at maximum speed XXXX is the distance in mm

6XXXXYYY Move forward  at speed YYY for the XXXX distance in mm (YYY=0...255, below a given threshold it does not move).

7XXXXYYY Move backward at speed YYY for the XXXX distance in mm (YYY=0...255, below a given threshold it does not move).

9        Bumping and falling Sensors state

C000XXXX Move the Right wheel for a circular movement with the left  wheel stopped for calibracion XXXX distance in cm

D000XXXX Move the Left  wheel for a circular movement with the right wheel stopped for calibracion XXXX distance in cm



<  Reset the Robot Global Position

>  Return the version code

?  Stop motors and calculate new positions

:  Read Ultrasound sensors ::> The Answer is US Sensors: XXX YYY ZZZ (XXX,YYY and ZZZ are the read distance in cm).

A  Return Robot Global position

B  Rerturn the state of the automata

E  Return the error code

F  Return the ultra-sound distances during a C or D command

G  >>>>>>>>     Return last distance from ultra-sound sensor  "documentar"

H  >>>>>>>>     Set Robot Global Position HXXXXYYYYTTT XXXX,YYYY es la posiciÃ³n en cm y TTT la orientaciÃ³n en grados

I

L

N Return the absolute encoders and the time-stamp, the actual format can be consulted in lola.ino file (enviar_datos_encoder).

M Devolver MON cuando se activa el sensor de movimiento y MOF cuando se desactiva la seÃ±al.

Q Return encoder values


2. Bluetooth functionality

The GRAM platform can be controlled using the same commands as USB via Bluetooth conection.
In the DOC folder you can find the document GRAM_PLATFORM_Bluetooth.

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
